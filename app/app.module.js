'use strict';

// Définition de l'application du site et les modules AngularJS à charger
var app = angular.module( 'Angular', [ 'ngRoute', 'ngMaterial', 'firebase' ] );

// Définitions des thèmes de couleurs pour le site
app.config(["$routeProvider", "$mdThemingProvider", function ($routeProvider, $mdThemingProvider) {
  // Définit les couleurs du thème par défaut
  var colorDark = $mdThemingProvider.extendPalette('brown', {
    '500': '#6d4c41',
    'contrastDefaultColor': 'light'
  });
  var colorLight = $mdThemingProvider.extendPalette('blue-grey', {
    'default': '#546e7a',
    'contrastDefaultColor': 'dark'
  });
  // Enregistre les couleurs du thème par défaut
  $mdThemingProvider.definePalette('colorPrimary', colorDark);
  $mdThemingProvider.definePalette('colorAccent', colorLight);
  // Définit le thème principal du site
  $mdThemingProvider.theme('default')
    .primaryPalette('colorPrimary')
    .accentPalette('colorAccent')
    .warnPalette('red')
    .backgroundPalette('brown', {
      'default': '200'
    });
  // Définit le thème "foncé" du site
  $mdThemingProvider.theme('darker').dark();
  // Définit le thème des cartes du site
  $mdThemingProvider.theme('card').backgroundPalette('grey');
  // Définit le thème utilisé par défaut
  $mdThemingProvider.setDefaultTheme('default');
  // Indique à l'application de surveiller le thème utilisé
  $mdThemingProvider.alwaysWatchTheme(true);
  // Active les couleurs du navigateur
  $mdThemingProvider.enableBrowserColor({
    theme: 'default'
  });
}]);

// Définition des dates au format français
app.config(function($mdDateLocaleProvider) {
	$mdDateLocaleProvider.firstDayOfWeek = 1;
	$mdDateLocaleProvider.formatDate = function(date) {
		return moment(date).format('DD-MM-YYYY');
	};
})

// Définitions des titres en fonction des routes utilisées
app.run(['$rootScope', function($rootScope) {
  $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
    if (!current.$$route) {
      current.$$route = 'Error';
      current.$$route.title = 'Error';
    }
    $rootScope.title = current.$$route.title;
  });
}]);
