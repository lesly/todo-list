'use strict';

// Services de manipulations des projets
app.factory('$projets', function($filter) {
  var $projets = {
    // Créer un projet
    creer: function(nomProjet, dateProjet) {
      // Génération du timestamp
      var d = new Date();
      var timestamp = d.getTime();
      firebase.database().ref('/projets/' + userConnect.uid + '/' + timestamp).set({
        id: timestamp,
        echeance: dateProjet,
        nom: nomProjet
      });
    },
    // Editer un projet
    editer: function(id, nom, date) {
      var updates = {};
      updates['nom'] = nom;
      updates['echeance'] = date;
      firebase.database().ref('/projets/' + userConnect.uid + '/' + id).update(updates);
    },
    // Supprimer un projet
    supprimer: function(id) {
      firebase.database().ref('/projets/' + userConnect.uid + '/' + id).remove();
    },
    // Liste des projets
    liste: function() {
      var projets = [];
      firebase.database().ref('/projets/' + userConnect.uid).orderByChild('id').on('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
          var date = $filter('date')(new Date(childSnapshot.val().echeance), "dd-MM-yyyy");
          projets.push({ id: childSnapshot.val().id, nom: childSnapshot.val().nom, date: childSnapshot.val().echeance, echeance: date });
        });
      });
      return projets;
    },
    // Liste des projets partagés
    partage: function() {
      var projets = [];
      firebase.database().ref('/projets/').orderByChild(userConnect.uid).on('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
          if (childSnapshot.key!=userConnect.uid) {
            firebase.database().ref('/projets/' + childSnapshot.key).orderByChild('id').on('value', function(snapshot) {
              snapshot.forEach(function(childSnapshot) {
                if (childSnapshot.child('collaborateurs/' + userConnect.uid).val()===true) {
                  var date = $filter('date')(new Date(childSnapshot.val().echeance), "dd-MM-yyyy");
                  projets.push({ id: childSnapshot.val().id, nom: childSnapshot.val().nom, date: childSnapshot.val().echeance, echeance: date, partage: true });
                }
              });
            });
          }
        });
      });
      return projets;
    },
  };
  return $projets;
});

// Services de manipulations des projets
app.factory('$taches', function() {
  var $taches = {
      // Créer une tache
      creer: function(nomTach, dateTache, descriptionTache, idProjet) {
          // Génération du timestamp
          var d = new Date();
          var timestamp = d.getTime();
          firebase.database().ref('taches/' + timestamp).set({
              id: timestamp,
              nom: nomTach,
              description: descriptionTache,
              echeance: dateTache,
              projet: idProjet,
              status: false
          });
      },
      // Editer une tache
      editer: function(nomTach, dateTache, descriptionTache, idTaches) {
          firebase.database().ref('taches/' + idTaches).update({
              nom: nomTach,
              description: descriptionTache,
              echeance: dateTache,
              status: false
          });
      },
      // Compléter une tache
      completer: function(idTaches) {
          firebase.database().ref('taches/' + idTaches).update({
              status: true
          });
      },
      // Supprimer une tache
      supprimer: function(idTaches) {
          // Génération du timestamp
          var d = new Date();
          var timestamp = d.getTime();
          firebase.database().ref('taches/' + idTaches).remove();
      },
    // Liste des taches d'un projet
    liste: function(id) {
      var taches = [];
      firebase.database().ref('taches/').orderByChild('id').on('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
          if (childSnapshot.val().projet==id) { taches.push(childSnapshot.val()); }
        });
      });
      return taches;
    },
  };
  return $taches;
});

// Services de manipulations des projets
app.factory('$collaborateurs', function() {
  var $collaborateurs = {
    // Liste des collaborateurs membre ou non d'un projet
    liste: function(id) {
      var collaborateurs = [];
      var collaborateur_nom, collaborateur_id;
      firebase.database().ref('/utilisateurs/').orderByChild('nom').on('value', function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
          collaborateur_nom = childSnapshot.child('nom').toJSON();
          collaborateur_id = childSnapshot.key;
          firebase.database().ref('/projets/' + userConnect.uid + '/' + id + '/collaborateurs/' + childSnapshot.key).on('value', function(snapshot) {
            if (snapshot.val()===true) { collaborateurs.push({ id: collaborateur_id, nom: collaborateur_nom, projet: true }); }
            else if (childSnapshot.key===userConnect.uid) {  }
            else { collaborateurs.push({ id: collaborateur_id, nom: collaborateur_nom, projet: false }); }
          });
        });
      });
      return collaborateurs;
    },
    // Ajout ou supprime le collaborateur d'un projet
    toggleProjet: function(projet, user, partage) {
      var updates = {};
      if (partage===true) {
        updates[user] = false;
        firebase.database().ref('/projets/' + userConnect.uid + '/' + projet + '/collaborateurs').update(updates);
      } else {
        updates[user] = true;
        firebase.database().ref('/projets/' + userConnect.uid + '/' + projet + '/collaborateurs').update(updates);
      }
    },
  };
  return $collaborateurs;
});
