'use strict';

// Directives pour l'include de la carte d'ajout
app.directive("mdAdd", function() { return { restrict: "E", templateUrl: "templates/add.tmpl.html" } });

// Directives pour les includes des cartes des projets
app.directive("mdProjet", function() { return { restrict: "E", templateUrl: "templates/carte.tmpl.html" } });
app.directive("mdProjetPartage", function() { return { restrict: "E", templateUrl: "templates/carte.tmpl.html" } });
