'use strict';

// Définitions des "routes" / pages pour le site
app.config(["$routeProvider", function ($routeProvider) {
    $routeProvider
    // PAGE D'ACCUEIL
    .when('/accueil', {
      title: 'Tous mes projets',
      templateUrl: 'views/accueil.html',
      controller: 'projets'
    })
    // PAGE DE CONNEXION
    .when('/connexion', {
      title: 'Se connecter',
      templateUrl: 'views/connexion.html',
      controller: 'connexion'
    })
    // SINON
    .otherwise({
      redirectTo: '/accueil'
    });
  }
]);
