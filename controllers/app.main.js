'use strict';

// Définition des fonctions globales
app.controller('main', function ($scope, $http, $mdSidenav, $mdToast, $mdDialog) {
  // Variables globales
  $scope.document_titre = 'ToDo List';
  $scope.document_connexion = 'Se connecter';
  $scope.meta_description = 'ToDo List';
  $scope.meta_auteur = 'Leslie Petrimaux & Grégoire Frémaux';
  $scope.version = 'version 1.0';
  $scope.btnSignUpGoogle = 's\'inscrire avec google';
  $scope.btnSignInGoogle = 'se connecter avec google';
  $scope.btnSignOutGoogle = 'se déconnecter avec google';
  // Variables de la sidenav
  $scope.menu_deconnexion = 'Se déconnecter';
  $scope.menu_accueil = 'Accueil';
  $scope.menu_projets = 'Tous les projets';
  $scope.menu_taches = 'Toutes mes tâches';
  $scope.menu_subhead_taches = 'Mes tâches';
  $scope.menu_taches_a_faire = 'À faire';
  $scope.menu_taches_personnelles = 'Personnelles';
  $scope.menu_taches_partagees = 'Partagées';
  // Variables d'action
  $scope.action_ouvrir_projets = 'ouvrir';
  // Nouvelle instance pour la base de donnée firebase
  var database = firebase.database();
  // Vérifie si l'utilisateur est connecté
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      // Utilisateur connecté
      $scope.isUserSignIng = user;
      $scope.nom_utilisateur = user.displayName;
    } else {
      // Utilisateur non connecté / redirection vers la page de connexion
      $scope.navigate('connexion');
      $scope.nom_utilisateur = null;
    }
  });
  // Fonction de connexion firebase via Google Auth
  $scope.firebaseGoogle = function (type) {
    // Nouvelle instance firebase pour l'authentification Google
    var provider = new firebase.auth.GoogleAuthProvider();
    // Fonction de connexion Google dans une pop-up
    firebase.auth().signInWithPopup(provider).then(function(result) {
      // Stocke le token et les infos de l'utilisateur
      token = result.credential.accessToken;
      userConnect = result.user;
      // Enregistrement de l'utilisateur dans firebase
      firebase.database().ref('utilisateurs/' + userConnect.uid).update({
        nom: userConnect.displayName,
        email: userConnect.email,
        photo : userConnect.photoURL
      });
      // Redirection vers la page d'accueil et affichage d'un toast pour informé du compte connecté
      $scope.navigate('accueil');
      $scope.displayToast('Vous êtes connecté en tant que : ' + userConnect.displayName, 5000);
    }).catch(function(error) {
      // Erreur lors de la connexion
      var errorCode = error.code;
      var errorMessage = error.message;
      var email = error.email;
      var credential = error.credential;
      // Affiche un toast retournant le message d'erreur
      $scope.displayToast('Erreur ' + errorCode + ' : ' + errorMessage, 5000);
    });
  };
  // Fonction de déconnexion firebase
  $scope.deconnexionGoogle = function () {
    // Déconnecte l'utilisateur
    firebase.auth().signOut().then(function() {
      // Si l'utilisateur a été correctement déconnecté, vide les variable & redirige vers la page de connexion
      userConnect = null;
      $scope.isUserSignIng = null;
      $scope.navigate('connexion');
      $scope.displayToast('Vous êtes maintenant déconnecté !', 5000);
    }).catch(function(error) {
      // Erreur lors de la déconnexion
      var errorCode = error.code;
      var errorMessage = error.message;
      var email = error.email;
      var credential = error.credential;
      // Affiche un toast retournant le message d'erreur
      $scope.displayToast('Erreur ' + errorCode + ' : ' + errorMessage, 5000);
    });
  };
  // Fonctions pour ouvrir les sidenav (gauche/droite)
  $scope.toggleLeft = buildToggler('left');
  $scope.toggleRight = buildToggler('right');
  function buildToggler(componentId) {
    return function() {
      $mdSidenav(componentId).toggle();
    };
  }
  // Navigation à partir du menu dans le site
  $scope.navigateTo = function (page) {
    window.location.href = '/#!/' + page;
    $mdSidenav('left').toggle(); // Ferme la sidenav lors du clic
  };
  // Navigation dans le site
  $scope.navigate = function (page) {
    window.location.href = '/#!/' + page;
  };
  // Retour à la page précédente
  $scope.retour = function() {
    window.history.back();
  };
  // Définit le sens des tooltips
  $scope.tooltipDir = {
    top: 'top',
    bottom: 'bottom',
    left: 'left',
    right: 'right'
  };
  // Affichage des Toasts
  $scope.displayToast = function (message, time) {
    var pinTo = $scope.getToastPosition();
    $mdToast.show(
      $mdToast.simple()
        .textContent(message)
        .position(pinTo)
        .hideDelay(time)
    );
  };
  // Définit la position des toasts
  var last = {
    bottom: false,
    top: true,
    left: false,
    right: true
  };
  // Retourne la position des toasts
  $scope.toastPosition = angular.extend({},last);
  $scope.getToastPosition = function() {
    sanitizePosition();
    return Object.keys($scope.toastPosition)
      .filter(function(pos) { return $scope.toastPosition[pos]; })
      .join(' ');
  };
  function sanitizePosition() {
    var current = $scope.toastPosition;
    if ( current.bottom && last.top ) current.top = false;
    if ( current.top && last.bottom ) current.bottom = false;
    if ( current.right && last.left ) current.left = false;
    if ( current.left && last.right ) current.right = false;
    last = angular.extend({},current);
  }
});

// Définition des fonctions pour la page de connexion
app.controller('connexion', function($scope) {
  // Détecte si l'utilisateur est connecté
  if ($scope.isUserSignIng) {
    $scope.navigate('accueil');
  } else {
    $scope.navigate('connexion');
  }
});
