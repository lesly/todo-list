'use strict';

// Définition des fonctions pour la page d'affichage de tous mes projets
app.controller('projets', function($scope, $mdDialog, $projets, $collaborateurs, $taches, $timeout, $filter) {
  // Variables pour la barre de navigation
  $scope.navigation = [
    {nom:'mes projets',lien:'projets', current:true}
  ];
  // Variables globales
  $scope.action_ajouter = 'Ajouter un projet';
  $scope.action_editer = 'Éditer';
  $scope.action_supprimer = 'Supprimer';
  $scope.action_ajouter_tache = 'ajouter une tâche';
  $scope.tooltip_collaborateurs = 'Gérer les collaborateurs';
  $scope.date_echeance = 'Date d\'échéance : ';
  // Liste des projets de l'utilisateur
  chargerProjets();
  function chargerProjets() {
    var variableProjets = null;
    var variableAutresProjets = null;
    $scope.mesProjets = null;
    $scope.autresProjets = null;
    $scope.pProjet = null;
    $scope.pAProjet = null;
    variableProjets = $projets.liste();
    $timeout( function(){ $scope.mesProjets = variableProjets; $scope.pProjet = true; }, 1000 );
    variableAutresProjets = $projets.partage();
    $timeout( function(){ $scope.autresProjets = variableAutresProjets; $scope.pAProjet = true; }, 1000 );
  }
  // Liste des taches par projet
  $scope.mesTaches = [];
  $scope.tachesProjets = function (id) {
    var variableTaches = $taches.liste(id);
    $timeout( function(){ $scope.mesTaches[id] = variableTaches; $scope.pTache = true; }, 1000 );
  };
  // Création d'un nouveau projet
  $scope.creer = function(ev) {
    $mdDialog.show({
      controller: creerController,
      templateUrl: 'templates/projet.tmpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
    })
    .then(function() {
      // Sauvegarde
    }, function() {
      // Action annulée
    });
  };
  function creerController($scope, $mdDialog, $projets, $filter) {
    // Titre de la page
    $scope.titre_page = 'Création d\'un projet';
    // Variables des boutons d'action
    $scope.action_enregistrer = 'enregistrer';
    $scope.action_retour = 'annuler et revenir à la page précédente';
    // Variables du bloc de création
    $scope.projet = [];
    var date = new Date();
    $scope.projet.date = date;
    $scope.$watch('projet.date', function(newVal, oldVal) {
  		if (!newVal) { return false; }
      date = $filter('date')(new Date(newVal), "yyyy-MM-dd");
  	});
    $scope.sauvegarder = function() {
      $projets.creer($scope.projet.nom, date);
      chargerProjets();
      $mdDialog.hide();
    };
    $scope.hide = function() {
      $mdDialog.hide();
    };
    $scope.cancel = function() {
      $mdDialog.cancel();
    };
  }
  // Edition d'un projet
  $scope.editer = function(ev, item) {
    $mdDialog.show({
      controller: editerController,
      templateUrl: 'templates/projet.tmpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
			locals: { item: item }
    })
    .then(function() {
      // Sauvegarde
    }, function() {
      // Action annulée
    });
  };
  function editerController($scope, $mdDialog, $projets, $filter, item) {
    // Stocke l'object dans un scope
    $scope.projet = item;
    // Titre de la page
    $scope.titre_page = 'Modification du projet : ' + $scope.projet.nom;
    // Variables des boutons d'action
    $scope.action_enregistrer = 'enregistrer';
    $scope.action_supprimer = 'supprimer';
    // Variables du bloc de création
    $scope.edition = true;
    var date = $scope.projet.date
    $scope.$watch('projet.date', function(newVal, oldVal) {
  		if (!newVal) { return false; }
      date = $filter('date')(new Date(newVal), "yyyy-MM-dd");
  	});
    $scope.sauvegarder = function() {
      $projets.editer($scope.projet.id, $scope.projet.nom, date);
      chargerProjets();
      $mdDialog.hide();
    };
    $scope.supprimer = function() {
      $projets.supprimer($scope.projet.id);
      chargerProjets();
      $mdDialog.hide();
    };
    $scope.hide = function() {
      $mdDialog.hide();
    };
    $scope.cancel = function() {
      $mdDialog.cancel();
    };
  }
  // Supprimer un projet
  $scope.supprimer = function(id) {
    $projets.supprimer(id);
    chargerProjets();
    $mdDialog.hide();
  };
  // Liste des collaborateurs
  $scope.collaborateurs = function(ev, id, projet) {
    $mdDialog.show({
      controller: collaborateursController,
      templateUrl: 'templates/collaborateurs.tmpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
			locals: { item: id, nom: projet }
    })
    .then(function() {
      // Sauvegarde
    }, function() {
      // Action annulée
    });
  };
  function collaborateursController($scope, $mdDialog, $collaborateurs, item, nom) {
    // Titre de la page
    $scope.titre_page = 'Gérer les membres du projet : ' + nom;
    // Variables du bloc d'édition
    $scope.infos_ajouter = 'Pour ajouter un collaborateur au projet, il vous suffit de cocher sa case.';
    chargerCollaborateurs();
    function chargerCollaborateurs () {
      $scope.mesCollaborateurs = null;
      $scope.pCollaborateur = null;
      var variableCollaborateurs = $collaborateurs.liste(item);
      $timeout( function(){ $scope.mesCollaborateurs = variableCollaborateurs; $scope.pCollaborateur = true; }, 1000 );
    }
    $scope.toggle = function(user, user_nom, partage) {
      $collaborateurs.toggleProjet(item, user, partage);
      chargerCollaborateurs();
      chargerProjets();
    };
    $scope.hide = function() {
      $mdDialog.hide();
    };
    $scope.cancel = function() {
      $mdDialog.cancel();
    };
  }
  // Création d'une nouvelle tâche
  $scope.creerTache = function(ev, id) {
      $mdDialog.show({
          controller: creerTache,
          templateUrl: 'templates/tache.tmpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
          locals: { item: id }
      })
      .then(function() {
          // Sauvegarde
      }, function() {
          // Action annulée
      });
  };
  function creerTache($scope, $mdDialog, $taches, $filter, item) {
      // Titre de la page
      $scope.titre_page = 'Création d\'une tâche';
      // Variables des boutons d'action
      $scope.action_enregistrer = 'enregistrer';
      $scope.action_retour = 'annuler et revenir à la page précédente';
      // Variables du bloc de création
      $scope.taches = [];
      var date = new Date();
      $scope.taches.echeance = date;
      $scope.$watch('tache.echeance', function(newVal, oldVal) {
          if (!newVal) { return false; }
          date = $filter('date')(new Date(newVal), "yyyy-MM-dd");
      });
      $scope.sauvegarder = function() {
          $taches.creer($scope.tache.nom, date, $scope.tache.description, item);
          chargerProjets();
          $mdDialog.hide();
      };
      $scope.hide = function() {
          $mdDialog.hide();
      };
      $scope.cancel = function() {
          $mdDialog.cancel();
      };
  }
  // Editer une tache
  $scope.editerTache = function(ev, item, partage) {
      $mdDialog.show({
          controller: editerTache,
          templateUrl: 'templates/tache.tmpl.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
          locals: { item: item, proprietaire: partage }
      })
      .then(function() {
          // Sauvegarde
      }, function() {
          // Action annulée
      });
  };
  function editerTache($scope, $mdDialog, $taches, $filter, item, proprietaire) {
      // Titre de la page
      $scope.titre_page = 'Edition de la tâche';
      $scope.tache = item;
      $scope.edition = true;
      $scope.partage = proprietaire;
      // Variables des boutons d'action
      $scope.action_enregistrer = 'enregistrer';
      $scope.action_supprimer = 'Supprimer';
      $scope.action_retour = 'annuler et revenir à la page précédente';
      // Variables du bloc de création
      var date = $scope.tache.echeance;
      $scope.$watch('tache.echeance', function(newVal, oldVal) {
          if (!newVal) { return false; }
          date = $filter('date')(new Date(newVal), "yyyy-MM-dd");
      });
      $scope.sauvegarder = function() {
          $taches.editer($scope.tache.nom, date, $scope.tache.description, $scope.tache.id);
          chargerProjets();
          $mdDialog.hide();
      };
      $scope.supprimer = function(ev){
        var confirm = $mdDialog.confirm()
              .title('Supprimer une tâche')
              .textContent('Êtes-vous certain de vouloir supprimer cette tâche définitivement ?')
              .ariaLabel('Supprimer une tâche')
              .targetEvent(ev)
              .ok('oui')
              .cancel('non');
        $mdDialog.show(confirm).then(function() {
          $taches.supprimer($scope.tache.id);
          chargerProjets();
          $mdDialog.hide();
        }, function() {
          $mdDialog.hide();
        });
      }
      $scope.hide = function() {
          $mdDialog.hide();
      };
      $scope.cancel = function() {
          $mdDialog.cancel();
      };
  }
  // Compléter une tache
  $scope.completer = function(ev, id) {
    var confirm = $mdDialog.confirm()
          .title('Achever une tâche')
          .textContent('Êtes-vous certain de vouloir marquer cette tâche comme terminée ?')
          .ariaLabel('Compléter une tâche')
          .targetEvent(ev)
          .ok('oui')
          .cancel('non');
    $mdDialog.show(confirm).then(function() {
      $taches.completer(id);
      chargerProjets();
      $mdDialog.hide();
    }, function() {
      $mdDialog.hide();
    });
  };
  // Supprimer une tâche
  $scope.supprimerTache = function(ev, id){
    var confirm = $mdDialog.confirm()
          .title('Supprimer une tâche')
          .textContent('Êtes-vous certain de vouloir supprimer cette tâche définitivement ?')
          .ariaLabel('Supprimer une tâche')
          .targetEvent(ev)
          .ok('oui')
          .cancel('non');
    $mdDialog.show(confirm).then(function() {
      $taches.supprimer(id);
      chargerProjets();
    }, function() {
    });
  }
});
